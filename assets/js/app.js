// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
import "../css/app.css"
import "../css/shapes.css"

// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
// import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
// Establish Phoenix Socket and LiveView configuration.
import {Socket} from "phoenix"
import {LiveSocket} from "phoenix_live_view"
import topbar from "../vendor/topbar"

const audioContext = new AudioContext();

let fanfareAudioElements;
let gameOverAudioElements;
let themesAudioElements;

let Hooks = {};
Hooks.loadAudio = {
  mounted() {
    this.pushEvent("loading_assets", {value: 0, text: "Loading fanfares"});
    Promise.all([
      fetch("/sounds/effects/Fanfare_1.wav"),
      fetch("/sounds/effects/Fanfare_2.wav"),
      fetch("/sounds/effects/Fanfare_3.wav")
    ])
    .then(fanfares => Promise.all(fanfares.map(fanfare => fanfare.text())))
    .then(fanfares => {
      fanfareAudioElements = fanfares.map(fanfare => new Audio(fanfare));

      const fanfareTracks = fanfareAudioElements.map(elem => audioContext.createMediaElementSource(elem));

      fanfareTracks.forEach(track => track.connect(audioContext.destination));

      this.pushEvent("loading_assets", {value: 33, text: "Loading game overs"});

      return Promise.all([
        fetch("/sounds/effects/Game_Over_1.wav"),
        fetch("/sounds/effects/Game_Over_2.wav"),
        fetch("/sounds/effects/Game_Over_3.wav")
      ]);
    })
    .then(gameOvers => Promise.all(gameOvers.map(gameOver => gameOver.text())))
    .then(gameOvers => {
      gameOverAudioElements = gameOvers.map(gameOver => new Audio(gameOver));

      const gameOverTracks = gameOverAudioElements.map(elem => audioContext.createMediaElementSource(elem));

      gameOverTracks.forEach(track => track.connect(audioContext.destination));

      this.pushEvent("loading_assets", {value: 33, text: "Loading themes"});
      return Promise.all([
        fetch("/sounds/themes/field_theme_1.wav")
      ])
    })
    .then(themes => Promise.all(themes.map(theme => theme.text())))
    .then(themes => {
      themesAudioElements = themes.map(theme => new Audio(theme));

      const themeTracks = themesAudioElements.map(elem => audioContext.createMediaElementSource(elem));

      themeTracks.forEach(track => track.connect(audioContext.destination));

      this.pushEvent("loading_assets", {value: 34, text: "Done"});
    })
    .catch(error => console.error("Error in fetching sounds: ", error));
  }
}

Hooks.playAudio = {
  mounted() {
    fanfareAudioElements[0].play();
  }
}

Hooks.playTheme = {
  mounted() {
    themesAudioElements[0].addEventListener('ended', function() {
      themesAudioElements[0].play();
    }, false);
    themesAudioElements[0].play();
    const element = document.getElementById("welcome-text");
    element.style="display: none;";
  }
}

Hooks.openDoorAudio = {
  mounted() {
    const pushEvent = (event, options) => {
      this.pushEvent(event, options);
    };

    const timeout = setTimeout(() => pushEvent("door_opened", {}), 5000);

    fanfareAudioElements[2].addEventListener('ended', function() {
      fanfareAudioElements[2].currentTime = 0;
    }, false);
    fanfareAudioElements[2].addEventListener('timeupdate', function() {
      if (fanfareAudioElements[2].currentTime >= 4) {
        clearTimeout(timeout);
        pushEvent("door_opened", {});
      }
    }, false);
    fanfareAudioElements[2].play();
  }
}

Hooks.nextLevel = {
  mounted() {
    setTimeout(() => this.pushEvent("next_level", {}), 2000);
  }
}

// Disable scroll on webpage using keyboard
// https://stackoverflow.com/questions/8916620/disable-arrow-key-scrolling-in-users-browser
window.addEventListener("keydown", function(e) {
  if(["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
    e.preventDefault();
  }
}, false);

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, {params: {_csrf_token: csrfToken}, hooks: Hooks})

// Show progress bar on live navigation and form submits
topbar.config({barColors: {0: "#29d"}, shadowColor: "rgba(0, 0, 0, .3)"})
window.addEventListener("phx:page-loading-start", info => topbar.show())
window.addEventListener("phx:page-loading-stop", info => topbar.hide())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket

