defmodule Master.Player.Control do
  alias Master.MapGenerator

  @spec move_player(
          map :: Map,
          current_row :: integer,
          current_column :: integer,
          new_row :: integer,
          new_column :: integer,
          player_value :: String
        ) :: Map
  def move_player(map, current_row, current_column, new_row, new_column, player_value) do
    game_constants = Application.get_env(:master, :game_constants) |> Enum.into(%{})

    is_flipped? =
      {new_column < current_column and player_value == "P",
       new_column < current_column and player_value == "C"}

    other_player_value = if player_value == "P", do: "C", else: "P"
    other_player_position = MapGenerator.get_player_position(map, other_player_value)

    map =
      MapGenerator.change_map_value(
        map,
        current_row,
        current_column,
        %{
          value: 0,
          explored?: true,
          visible?: MapGenerator.get_map_cell_value(map, current_row, current_column).visible?
        },
        is_flipped?
      )

    map =
      MapGenerator.change_map_value(
        map,
        current_row,
        current_column,
        %{
          rendered_tile:
            MapGenerator.Renderer.get_rendered_tile(
              map,
              current_row,
              current_column,
              MapGenerator.get_map_cell_value(map, current_row, current_column),
              is_flipped?
            )
        },
        is_flipped?
      )

    map =
      MapGenerator.change_map_value(
        map,
        new_row,
        new_column,
        %{
          value: player_value,
          explored?: true,
          visible?: true
        },
        is_flipped?
      )

    map =
      MapGenerator.change_map_value(
        map,
        new_row,
        new_column,
        %{
          rendered_tile:
            MapGenerator.Renderer.get_rendered_tile(
              map,
              new_row,
              new_column,
              MapGenerator.get_map_cell_value(map, new_row, new_column),
              is_flipped?
            )
        },
        is_flipped?
      )

    map = MapGenerator.explore_around_point(map, new_row, new_column, game_constants.light_radius)

    map =
      MapGenerator.set_visible_around_point(
        map,
        new_row,
        new_column,
        true,
        game_constants.light_radius,
        is_flipped?
      )

    map =
      case other_player_position do
        {nil, nil} ->
          map

        {other_player_row, other_player_column} ->
          map =
            MapGenerator.explore_around_point(
              map,
              other_player_row,
              other_player_column,
              game_constants.light_radius
            )

          MapGenerator.set_visible_around_point(
            map,
            other_player_row,
            other_player_column,
            false,
            game_constants.light_radius,
            is_flipped?
          )
      end

    map
  end

  def is_move_valid?(map, row, column) do
    Enum.any?(map, fn {row_index, row_value} ->
      if row_index == row do
        {column, %{value: 0, explored?: true, visible?: true}} ==
          Enum.find_value(row_value, fn {column_index, column_value} ->
            if column_index == column and column_value.value == 0 do
              {column_index, %{value: column_value.value, explored?: true, visible?: true}}
            else
              false
            end
          end)
      else
        false
      end
    end)
  end
end
