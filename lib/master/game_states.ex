defmodule Master.GameStates do
  import Ecto.Query

  alias Master.Helpers
  alias Master.Repo
  alias Master.GameStates.GameState
  alias Master.GameStates.GameMessage
  alias Master.GameStates.GamePlayer

  def get_game_state_by_uuid(uuid) do
    uuid = if is_integer(uuid), do: Integer.to_string(uuid), else: uuid

    game =
      from(
        game_state in GameState,
        preload: [:messages, :players],
        where: game_state.uuid == ^uuid
      )
      |> Repo.one()

    if not is_nil(game) do
      game = Map.from_struct(game)

      messages = Enum.map(game.messages, fn message -> Map.from_struct(message) end)

      Map.put(game, :messages, messages)
    else
      nil
    end
  end

  def get_player_by_id(id), do: Repo.get(GamePlayer, id)

  def insert_player_to_game(uuid, player) do
    uuid = if is_integer(uuid), do: Integer.to_string(uuid), else: uuid
    game_state = get_game_state_by_uuid(uuid)

    existing_player =
      Enum.find(game_state.players, fn p ->
        p.child? == player.child? and p.name == player.name
      end)

    if not is_nil(existing_player) do
      existing_player
    else
      %GamePlayer{}
      |> GamePlayer.changeset(player |> Map.merge(%{game_state_id: game_state.id}))
      |> Repo.insert!()
    end

    # Repo.get(GamePlayer, id)
  end

  def save_game_state(uuid, state) do
    uuid = if is_integer(uuid), do: Integer.to_string(uuid), else: uuid

    struct_map =
      if not is_nil(state.map) do
        Enum.map(state.map, fn {_row_index, row_value} ->
          Enum.map(row_value, fn {_column_index, column_value} ->
            Helpers.string_to_atom_key_map(column_value)
          end)
          |> Enum.with_index(0)
          |> Enum.map(fn {column_value, column_index} -> {column_index, column_value} end)
          |> Map.new()
        end)
        |> Enum.with_index(0)
        |> Enum.map(fn {row_value, row_key} -> {row_key, row_value} end)
        |> Map.new()
      else
        nil
      end

    new_game_state = %{
      uuid: uuid,
      map: struct_map
    }

    game_state = Repo.get_by(GameState, uuid: uuid)

    if is_nil(game_state) do
      new_game_state =
        %GameState{}
        |> GameState.changeset(new_game_state)
        |> Repo.insert!()

      new_players =
        Enum.map(state.players, fn player ->
          %GamePlayer{}
          |> GamePlayer.changeset(Map.merge(player, %{game_state_id: new_game_state.id}))
          |> Repo.insert!()
        end)

      new_messages =
        Enum.map(state.messages, fn message ->
          %GameMessage{}
          |> GameMessage.changeset(Map.merge(message, %{game_state_id: new_game_state.id}))
          |> Repo.insert!()
        end)

      Map.merge(new_game_state, %{players: new_players, messages: new_messages})
    else
      updated_game_state =
        game_state
        |> GameState.changeset(new_game_state)
        |> Repo.update!()

      updated_players =
        Enum.map(state.players, fn player ->
          params =
            if is_struct(player),
              do: Map.from_struct(player),
              else:
                player
                |> Map.merge(%{game_state_id: updated_game_state.id})

          player
          |> GamePlayer.changeset(params)
          |> Repo.insert_or_update!()
        end)

      Map.merge(updated_game_state, %{players: updated_players})
    end
  end

  def update_player_points(player, points) do
    player = get_player_by_id(player.id)

    points = player.points + points

    player
    |> GamePlayer.changeset(%{points: points})
    |> Repo.update()
  end
end
