defmodule Master.Repo do
  use Ecto.Repo,
    otp_app: :master,
    adapter: Ecto.Adapters.Postgres

  import Ecto.Query

  alias __MODULE__

  def load_log_file() do
    file_name = Application.get_env(:master, :log_file)

    File.open!(file_name, [:append])
  end

  def write_to_file(file, content) do
    IO.binwrite(file, "#{content}\n")
  end

  def row_exists?(module, key, value) do
    from(module)
    |> Repo.all()
    |> Enum.map(fn row -> Map.get(row, key) == value end)
    |> Enum.filter(fn row -> row end)
    |> Kernel.length() > 0
  end

  def row_exists?(module, key1, value1, key2, value2) do
    from(module)
    |> Repo.all()
    |> Enum.map(fn row -> Map.get(row, key1) == value1 and Map.get(row, key2) == value2 end)
    |> Enum.filter(fn row -> row end)
    |> Kernel.length() > 0
  end
end
