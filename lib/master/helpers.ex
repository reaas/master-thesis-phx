defmodule Master.Helpers do
  def atom_to_string_key_map(map) when is_map(map) do
    for {key, val} <- map, into: %{} do
      if is_atom(key) do
        {Atom.to_string(key), val}
      else
        {key, val}
      end
    end
  end

  def atom_to_string_key_map(list) when is_list(list) do
    Enum.map(list, &atom_to_string_key_map/1)
  end

  def string_to_atom_key_map(map) when is_map(map) do
    for {key, val} <- map, into: %{} do
      if is_binary(key) do
        {String.to_atom(key), val}
      else
        {key, val}
      end
    end
  end

  def string_to_atom_key_map(list) when is_list(list) do
    Enum.map(list, &string_to_atom_key_map/1)
  end
end
