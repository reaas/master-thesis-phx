defmodule Master.Levels do
  alias Master.Repo
  alias Master.Levels.Level

  def get_level_by_level_number(level_number) do
    Repo.get_by(Level, number: level_number)
  end

  def create_level(params) do
    %Level{}
    |> Level.changeset(params)
    |> Repo.insert()
  end
end
