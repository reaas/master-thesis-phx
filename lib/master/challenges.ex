defmodule Master.Challenges do
  import Ecto.Query

  alias Master.Repo
  alias Master.Challenges.Challenge
  alias Master.Challenges.ChallengeAnswer
  alias Master.Challenges.ChallengeOption

  def get_challenge(id), do: Repo.get(Challenge, id)
  def get_challenge_option(id), do: Repo.get(ChallengeOption, id)

  def get_random_challenge() do
    from(
      Challenge,
      order_by: fragment("RANDOM()"),
      preload: [:options],
      limit: 1
    )
    |> Repo.one()
  end

  def update_option_answered(challenge_answers) do
    Enum.each(challenge_answers, fn challenge_answer ->
      if challenge_answer["answered?"] do
        challenge_option = get_challenge_option(challenge_answer["id"])

        challenge_option
        |> ChallengeOption.changeset(%{times_answered: challenge_option.times_answered + 1})
        |> Repo.update()
      end
    end)
  end

  def add_challenge_answer(challenge_answer) do
    %ChallengeAnswer{}
    |> ChallengeAnswer.changeset(challenge_answer)
    |> Repo.insert()
  end

  def create_challenge(params, options) do
    challenge = Challenge.changeset(%Challenge{}, params)

    challenge = Repo.insert!(challenge)

    options
    |> Enum.map(fn option ->
      option = Map.merge(option, %{challenge_id: challenge.id})

      ChallengeOption.changeset(%ChallengeOption{}, option)
      |> Repo.insert!()
    end)
  end
end
