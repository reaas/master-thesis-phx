defmodule Master.Questioneer do
  import Ecto.Query

  alias Master.Repo
  alias Master.Questioneer.QuestioneerAnswer
  alias Master.Questioneer.QuestioneerQuestion
  alias Master.Questioneer.QuestioneerQuestionOption

  def get_questioneer_questions(questioneer_type) do
    from(
      qq in QuestioneerQuestion,
      where: qq.questioneer_type == ^questioneer_type,
      preload: [
        options:
          ^from(
            qqo in QuestioneerQuestionOption,
            order_by: [asc: qqo.text]
          )
      ]
    )
    |> Repo.all()
  end

  def add_questioneer_answers(answers) do
    Enum.map(answers, fn answer -> add_questioneer_answer(answer) end)
  end

  def add_questioneer_answer(answer) do
    %QuestioneerAnswer{}
    |> QuestioneerAnswer.changeset(answer)
    |> Repo.insert()
  end

  def create_questioneer_question(params, options) do
    questioneer_question = QuestioneerQuestion.changeset(%QuestioneerQuestion{}, params)

    questioneer_question = Repo.insert!(questioneer_question)

    options
    |> Enum.map(fn option ->
      option = Map.merge(option, %{questioneer_question_id: questioneer_question.id})

      QuestioneerQuestionOption.changeset(%QuestioneerQuestionOption{}, option)
      |> Repo.insert!()
    end)
  end
end
