defmodule Master.SoundRepo do
  def get_effects_file(filename) do
    env = Application.get_env(:master, :environment)
    local_path = Application.get_env(:master, :sound_effects_path)

    if env == :prod or (not File.exists?(local_path <> filename) and env == :dev) do
      assets_uri = Application.get_env(:master, :assets_uri)
      response = HTTPoison.get!("#{assets_uri}/sounds/effects/#{filename}")

      # Save file locally if dev
      if not File.exists?(local_path <> filename) and env == :dev do
        File.write!(local_path <> filename, response.body)
      end

      response.body
      |> Base.encode64()
    else
      path = Application.get_env(:master, :sound_effects_path)

      File.read!(path <> filename)
      |> Base.encode64()
    end
  end

  def get_theme_file(filename) do
    env = Application.get_env(:master, :environment)
    local_path = Application.get_env(:master, :sound_themes_path)

    if env == :prod or (not File.exists?(local_path <> filename) and env == :dev) do
      assets_uri = Application.get_env(:master, :assets_uri)
      response = HTTPoison.get!("#{assets_uri}/sounds/themes/#{filename}")

      # Save file locally if dev
      if not File.exists?(local_path <> filename) and env == :dev do
        File.write!(local_path <> filename, response.body)
      end

      response.body
      |> Base.encode64()
    else
      File.read!(local_path <> filename)
      |> Base.encode64()
    end
  end
end
