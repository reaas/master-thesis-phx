defmodule Master.MapGenerator.Renderer do
  alias Master.MapGenerator

  def get_rendered_tile(map, row, column, cell, is_flipped? \\ {false, false}) do
    neighbours = MapGenerator.get_neighbours(map, row, column)

    {tile_image_path, extra_style} =
      case cell.value do
        0 -> render_floor(neighbours, row, column)
        1 -> render_wall(neighbours)
        "P" -> render_player(is_flipped? |> elem(0))
        "C" -> render_child(is_flipped? |> elem(1))
        "D" -> render_door()
        "O" -> {nil, nil}
        _ -> {nil, nil}
      end

    style =
      cond do
        not cell.explored? and tile_image_path -> "opacity: 0%;"
        not cell.visible? and tile_image_path -> "opacity: 50%;"
        true -> ""
      end

    style =
      case not is_nil(extra_style) do
        true -> "#{style} #{extra_style}"
        false -> "#{style}"
      end

    if not is_nil(tile_image_path) do
      "<img src='#{tile_image_path}' style='#{style}' />"
    else
      ""
    end
  end

  defp render_door() do
    {"/images/tiles/wall/door_closed.png", "max-width: 16px;"}
  end

  defp render_floor(neighbours, row, column) do
    floor_tile_pool = [
      {"/images/renamed_sprites/path_1.png", nil},
      {"/images/renamed_sprites/path_2.png", nil},
      {"/images/renamed_sprites/path_3.png", nil},
      {"/images/renamed_sprites/path_4.png", nil},
      {"/images/renamed_sprites/path_5.png", nil},
      {"/images/renamed_sprites/path_6.png", nil},
      {"/images/renamed_sprites/path_7.png", nil},
      {"/images/renamed_sprites/path_8.png", nil},
      {"/images/renamed_sprites/path_9.png", nil},
      {"/images/renamed_sprites/path_10.png", nil}
    ]

    cond do
      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and
          is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/path_15.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.right.value) and
        is_floor_or_player?(neighbours.bottom.value) and
          neighbours.top.value == 1 ->
        {"/images/renamed_sprites/path_edge.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and
          neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/path_edge.png", "transform: rotate(180deg)"}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.bottom.value) and
          neighbours.right.value == 1 ->
        {"/images/renamed_sprites/path_edge.png", "transform: rotate(90deg)"}

      is_floor_or_player?(neighbours.right.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.bottom.value) and
          neighbours.left.value == 1 ->
        {"/images/renamed_sprites/path_edge.png", "transform: rotate(270deg)"}

      is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.bottom.value) and
        neighbours.right.value == 1 and neighbours.left.value == 1 ->
        {"/images/renamed_sprites/path_hallway.png", nil}

      is_floor_or_player?(neighbours.right.value) and
        is_floor_or_player?(neighbours.left.value) and
        neighbours.bottom.value == 1 and neighbours.top.value == 1 ->
        {"/images/renamed_sprites/path_hallway.png", "transform: rotate(90deg)"}

      neighbours.left.value == 1 and is_floor_or_player?(neighbours.top.value) and
        neighbours.right.value == 1 and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/path_3.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        neighbours.right.value == 1 and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/path_corner_5.png", nil}

      neighbours.left.value == 1 and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/path_corner_4.png", nil}

      neighbours.left.value == 1 and neighbours.top.value == 1 and
        is_floor_or_player?(neighbours.right.value) and
          is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/path_corner_2.png", nil}

      is_floor_or_player?(neighbours.left.value) and neighbours.top.value == 1 and
        neighbours.right.value == 1 and is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/path_corner_3.png", nil}

      neighbours.left.value == 1 and neighbours.top.value == 1 and
        is_floor_or_player?(neighbours.right.value) and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/path_8.png", nil}

      is_floor_or_player?(neighbours.left.value) and neighbours.top.value == 1 and
        neighbours.right.value == 1 and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/path_4.png", nil}

      neighbours.left.value == 1 and neighbours.top.value == 1 and neighbours.right.value == 1 and
          is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/path_3.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and neighbours.bottom.value == "O" ->
        {"/images/renamed_sprites/path_edge.png", "transform: rotate(180deg)"}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        neighbours.right.value == "O" and is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/path_edge.png", "transform: rotate(90deg)"}

      true ->
        {"/images/renamed_sprites/path_1.png", nil}
    end
  end

  defp render_wall(neighbours) do
    black_maybe? =
      Enum.filter(neighbours.list, fn neighbour ->
        neighbour.value == 1 or neighbour.value == "O"
      end)
      |> length()
      |> Kernel.==(4)

    cond do
      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and
          is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/stone_block.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.right.value) and
        is_floor_or_player?(neighbours.bottom.value) and
          neighbours.top.value == 1 ->
        {"/images/renamed_sprites/mountain_8.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and
          neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_2.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.bottom.value) and
          neighbours.right.value == 1 ->
        {"/images/renamed_sprites/mountain_1.png", nil}

      is_floor_or_player?(neighbours.right.value) and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.bottom.value) and
          neighbours.left.value == 1 ->
        {"/images/renamed_sprites/mountain_3.png", nil}

      is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.bottom.value) and
        neighbours.right.value == 1 and neighbours.left.value == 1 ->
        {"/images/renamed_sprites/mountain_2.png", nil}

      is_floor_or_player?(neighbours.right.value) and
        is_floor_or_player?(neighbours.left.value) and
        neighbours.bottom.value == 1 and neighbours.top.value == 1 ->
        {"/images/renamed_sprites/mountain_5.png", nil}

      neighbours.left.value == 1 and is_floor_or_player?(neighbours.top.value) and
        neighbours.right.value == 1 and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_2.png", nil}

      is_floor_or_player?(neighbours.left.value) and is_floor_or_player?(neighbours.top.value) and
        neighbours.right.value == 1 and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_1.png", nil}

      neighbours.left.value == 1 and is_floor_or_player?(neighbours.top.value) and
        is_floor_or_player?(neighbours.right.value) and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_3.png", nil}

      neighbours.left.value == 1 and neighbours.top.value == 1 and
        is_floor_or_player?(neighbours.right.value) and
          is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/mountain_9.png", nil}

      is_floor_or_player?(neighbours.left.value) and neighbours.top.value == 1 and
        neighbours.right.value == 1 and is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/mountain_7.png", nil}

      neighbours.left.value == 1 and neighbours.top.value == 1 and
        is_floor_or_player?(neighbours.right.value) and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_6.png", nil}

      is_floor_or_player?(neighbours.left.value) and neighbours.top.value == 1 and
        neighbours.right.value == 1 and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_4.png", nil}

      neighbours.left.value == 1 and neighbours.top.value == 1 and neighbours.right.value == 1 and
          is_floor_or_player?(neighbours.bottom.value) ->
        {"/images/renamed_sprites/mountain_8.png", nil}

      neighbours.left.value == 1 and is_floor_or_player?(neighbours.top.value) and
        neighbours.right.value == 1 and neighbours.bottom.value == "O" ->
        {"/images/renamed_sprites/mountain_2.png", nil}

      is_floor_or_player?(neighbours.left.value) and neighbours.top.value == 1 and
        neighbours.right.value == "O" and neighbours.bottom.value == 1 ->
        {"/images/renamed_sprites/mountain_4.png", nil}

      black_maybe? ->
        {"/images/tiles/wall/black.png", nil}

      true ->
        {"/images/tiles/wall/wall_1.png", nil}
    end
  end

  defp render_player(is_flipped?) do
    case is_flipped? do
      true ->
        {"/images/renamed_sprites/player_1.png", "transform: scaleX(-1); max-width: 16px;"}

      false ->
        {"/images/renamed_sprites/player_1.png", "max-width: 16px;"}
    end
  end

  defp render_child(is_flipped?) do
    case is_flipped? do
      true ->
        {"/images/renamed_sprites/player_2.png", "transform: scaleX(-1); max-width: 16px;"}

      false ->
        {"/images/renamed_sprites/player_2.png", "max-width: 16px;"}
    end
  end

  defp is_player?(value) do
    Enum.any?(["P", "C"], fn c -> c == value end)
  end

  defp is_floor_or_player?(value) do
    Enum.any?(["P", "C", "D", 0], fn c -> c == value end)
  end

  defp is_door?(value) do
    Enum.any?(["D"], fn c -> c == value end)
  end
end
