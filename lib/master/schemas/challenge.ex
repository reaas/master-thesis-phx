defmodule Master.Challenges.Challenge do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.Challenges.ChallengeOption

  schema "challenge" do
    field :header, :string
    field :multiple, :boolean
    field :text, :string
    field :active_status, :boolean, default: true

    has_many(:options, ChallengeOption, foreign_key: :challenge_id)

    timestamps()
  end

  def changeset(challenge, params \\ %{}) do
    challenge
    |> cast(params, [
      :header,
      :multiple,
      :text,
      :active_status
    ])
  end
end
