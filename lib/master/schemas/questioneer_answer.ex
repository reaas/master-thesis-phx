defmodule Master.Questioneer.QuestioneerAnswer do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.GameStates.GamePlayer
  alias Master.Questioneer.QuestioneerQuestion
  alias Master.Questioneer.QuestioneerQuestionOption

  schema "questioneer_answer" do
    field :answered_value, :string
    field :player_type, Ecto.Enum, values: [:CHILD, :PARENT]
    field :questioneer_type, Ecto.Enum, values: [:PRE, :POST]

    timestamps()

    belongs_to(:questioneer_question, QuestioneerQuestion)
    belongs_to(:questioneer_question_option, QuestioneerQuestionOption)
    belongs_to(:game_player, GamePlayer)
  end

  def changeset(questioneer_answer, params \\ %{}) do
    questioneer_answer
    |> cast(params, [
      :answered_value,
      :player_type,
      :questioneer_question_id,
      :questioneer_question_option_id,
      :game_player_id,
      :questioneer_type
    ])
  end
end
