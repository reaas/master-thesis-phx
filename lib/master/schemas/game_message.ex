defmodule Master.GameStates.GameMessage do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.GameStates.GameState

  schema "game_message" do
    field :user, :string
    field :content, :string

    timestamps()

    belongs_to(:game_state, GameState)
  end

  def changeset(game_message, params \\ %{}) do
    game_message
    |> cast(params, [
      :user,
      :content,
      :game_state_id
    ])
  end
end
