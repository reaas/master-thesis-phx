defmodule Master.Challenges.ChallengeAnswer do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.Challenges.Challenge
  alias Master.Challenges.ChallengeOption
  alias Master.GameStates.GamePlayer

  schema "challenge_answer" do
    belongs_to :challenge, Challenge
    belongs_to :challenge_option, ChallengeOption
    belongs_to :game_player, GamePlayer

    timestamps()
  end

  def changeset(challenge_answer, params \\ %{}) do
    challenge_answer
    |> cast(params, [
      :challenge_id,
      :challenge_option_id,
      :game_player_id
    ])
  end
end
