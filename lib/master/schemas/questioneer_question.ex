defmodule Master.Questioneer.QuestioneerQuestion do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.Questioneer.QuestioneerAnswer
  alias Master.Questioneer.QuestioneerQuestionOption

  schema "questioneer_question" do
    field :header, :string
    field :text, :string
    field :footer, :string
    field :multiple, :boolean
    field :questioneer_type, Ecto.Enum, values: [:PRE, :POST]
    field :active_status, :boolean, default: true

    timestamps()

    has_many(:options, QuestioneerQuestionOption)
    has_many(:answers, QuestioneerAnswer)
  end

  def changeset(questioneer_question, params \\ %{}) do
    questioneer_question
    |> cast(params, [
      :header,
      :text,
      :footer,
      :multiple,
      :questioneer_type,
      :active_status
    ])
  end
end
