defmodule Master.Levels.Level do
  use Ecto.Schema

  import Ecto.Changeset

  schema "level" do
    field :number, :integer
    field :required_child_points, :integer
    field :required_parent_points, :integer
    field :required_total_points, :integer
    field :dimension, :integer, default: 20
    field :max_tunnels, :integer, default: 50
    field :max_length, :integer, default: 8
    field :active_status, :boolean, default: true

    timestamps()
  end

  def changeset(level, params \\ %{}) do
    level
    |> cast(params, [
      :number,
      :required_child_points,
      :required_parent_points,
      :required_total_points,
      :dimension,
      :max_tunnels,
      :max_length,
      :active_status
    ])
  end
end
