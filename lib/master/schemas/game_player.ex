defmodule Master.GameStates.GamePlayer do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.GameStates.GameState
  alias Master.Questioneer.QuestioneerAnswer

  schema "game_player" do
    field :child?, :boolean
    field :name, :string
    field :points, :integer, default: 0

    timestamps()

    belongs_to(:game_state, GameState)
    has_many(:questioneer_answers, QuestioneerAnswer)
  end

  def changeset(game_player, params \\ %{}) do
    game_player
    |> cast(params, [
      :child?,
      :name,
      :game_state_id,
      :points
    ])
  end
end
