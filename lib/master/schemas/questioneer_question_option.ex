defmodule Master.Questioneer.QuestioneerQuestionOption do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.Questioneer.QuestioneerAnswer
  alias Master.Questioneer.QuestioneerQuestion

  schema "questioneer_question_option" do
    field :text, :string
    field :type, Ecto.Enum, values: [:string, :boolean, :integer]
    field :active_status, :boolean, default: true

    timestamps()

    belongs_to(:questioneer_question, QuestioneerQuestion)
    has_many(:answers, QuestioneerAnswer)
  end

  def changeset(questioneer_question_option, params \\ %{}) do
    questioneer_question_option
    |> cast(params, [
      :questioneer_question_id,
      :text,
      :type,
      :active_status
    ])
  end
end
