defmodule Master.GameStates.GameState do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.GameStates.GameMessage
  alias Master.GameStates.GamePlayer

  schema "game_state" do
    field :uuid, :string
    field :map, :map
    field :level, :integer, default: 1
    field :door_opening?, :boolean, default: false
    field :game_finished?, :boolean, default: false

    timestamps()

    has_many(:messages, GameMessage)
    has_many(:players, GamePlayer)
  end

  def changeset(game_state, params \\ %{}) do
    game_state
    |> cast(params, [
      :uuid,
      :map,
      :level,
      :door_opening?,
      :game_finished?
    ])
  end
end
