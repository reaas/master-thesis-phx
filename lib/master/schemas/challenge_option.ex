defmodule Master.Challenges.ChallengeOption do
  use Ecto.Schema

  import Ecto.Changeset

  alias Master.Challenges.Challenge

  schema "challenge_option" do
    belongs_to(:challenge, Challenge)

    field :points, :integer
    field :text, :string
    field :times_answered, :integer
    field :active_status, :boolean, default: true

    timestamps()
  end

  def changeset(challenge_option, params \\ %{}) do
    challenge_option
    |> cast(params, [
      :challenge_id,
      :points,
      :text,
      :times_answered,
      :active_status
    ])
  end
end
