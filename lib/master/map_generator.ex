defmodule Master.MapGenerator do
  alias Master.MapGenerator.Renderer

  @moduledoc """
  A procedural map generator. Creates randomly connected tunnels and rooms
  defined by the inputs to create_map
  """

  @doc """
  Creates a procedurally generated map. The dimensions are represented with one
  integer value, e.g. 5, which will return a 5x5 list containing the map.

  `max_tunnels` are the number of prefered tunnels in the map. The larger this value is compared
  to the dimension, the denser the map will be.

  `max_length` is the maximum length of each tunnel. The larger this value is compared to the
  dimension, the more "tunnel-y" the map will look.
  """
  @spec create_map(
          dimensions :: integer,
          max_tunnels :: integer,
          max_length :: integer,
          child? :: boolean
        ) :: [[]]
  def create_map(dimensions, max_tunnels, max_length, child?)
      when max_tunnels > 0 and not is_nil(max_length) do
    map = create_empty_map(1, dimensions)
    directions = [[-1, 0], [1, 0], [0, -1], [0, 1]]

    current_row = floor(:rand.uniform() * (dimensions + 1)) - 1
    current_column = floor(:rand.uniform() * (dimensions + 1)) - 1

    random_direction = find_random_direction(directions, [], [])

    random_length = ceil(:rand.uniform() * max_length)

    player_value = if child?, do: "C", else: "P"

    create_tunnel(
      map,
      dimensions,
      directions,
      random_direction,
      [],
      current_row,
      current_column,
      0,
      random_length,
      max_length,
      max_tunnels
    )
    |> create_end_position([], directions, nil, nil, [], "D", true)
    |> create_start_position(dimensions, directions, nil, nil, [], [], player_value, true)
    |> render_map()
  end

  @doc """
  Creates an empty map with the defined dimensions. With `char = 1` and `dimensions = 3`
  it will return a map on the form:
  ```
  %{
    0 => %{
      0 => %{value: char, explored?: false, visible?: false},
      1 => %{value: char, explored?: false, visible?: false},
      2 => %{value: char, explored?: false, visible?: false}
    },
    1 => %{
      0 => %{value: char, explored?: false, visible?: false},
      1 => %{value: char, explored?: false, visible?: false},
      2 => %{value: char, explored?: false, visible?: false}
    },
    2 => %{
      0 => %{value: char, explored?: false, visible?: false},
      1 => %{value: char, explored?: false, visible?: false},
      2 => %{value: char, explored?: false, visible?: false}
    }
  }

  The indexes are added to be used later in the map generation code
  """
  @spec create_empty_map(char :: String, dimensions :: integer) :: map
  def create_empty_map(char, dimensions) do
    for _ <- 0..dimensions do
      for _ <- 0..dimensions do
        char
      end
    end
    |> Enum.with_index(0)
    |> Enum.map(fn {row_value, row_key} ->
      columns =
        row_value
        |> Enum.with_index(0)
        |> Enum.map(fn {col_value, col_key} ->
          {col_key, %{value: col_value, explored?: false, visible?: false, rendered_tile: nil}}
        end)
        |> Map.new()

      {row_key, columns}
    end)
    |> Map.new()
  end

  defp find_random_direction(_directions, [rand_x, rand_y], [last_x, last_y])
       when (rand_x != -last_x and rand_y != -last_y) or (rand_x != last_x and rand_y != last_y) do
    [rand_x, rand_y]
  end

  defp find_random_direction(directions, _random_direction, []) do
    Enum.at(directions, floor(:rand.uniform() * length(directions)))
  end

  defp find_random_direction(directions, _random_direction, last_direction) do
    find_random_direction(
      directions,
      Enum.at(directions, floor(:rand.uniform() * length(directions))),
      last_direction
    )
  end

  # Recursive call that creates tunnels in the given map.
  # This function is called when all tunnels are created
  @spec create_tunnel(
          map :: Map,
          _dimensions :: integer,
          _directions :: [[]],
          _random_direction :: [],
          _last_direction :: [],
          _current_row :: integer,
          _current_column :: integer,
          _tunnel_length :: integer,
          _random_length :: integer,
          _max_length :: integer,
          _max_tunnels :: integer
        ) :: Map
  defp create_tunnel(map, _, _, _, _, _, _, _, _, _, 0) do
    map
  end

  # This function is called when one tunnel is finalized, and it initiates the
  # creation of the next, unit `max_tunnels` are 0.
  defp create_tunnel(
         map,
         dimensions,
         directions,
         random_direction,
         _last_direction,
         current_row,
         current_column,
         tunnel_length,
         random_length,
         max_length,
         max_tunnels
       )
       when tunnel_length == random_length do
    last_direction = random_direction
    random_direction = find_random_direction(directions, random_direction, last_direction)
    random_length = ceil(:rand.uniform() * max_length)

    create_tunnel(
      map,
      dimensions,
      directions,
      random_direction,
      last_direction,
      current_row,
      current_column,
      0,
      random_length,
      max_length,
      max_tunnels - 1
    )
  end

  # Ensures that the tunnel is within the map boundaries, and that is continues in the
  # same direction as before
  defp create_tunnel(
         map,
         dimensions,
         directions,
         [rand_x, rand_y],
         last_direction,
         current_row,
         current_column,
         tunnel_length,
         random_length,
         max_length,
         max_tunnels
       ) do
    if (current_row == 0 and rand_x == -1) or
         (current_column == 0 and rand_y == -1) or
         (current_row == dimensions - 1 and rand_x == 1) or
         (current_column == dimensions - 1 and rand_y == 1) do
      create_tunnel(
        map,
        dimensions,
        directions,
        [rand_x, rand_y],
        last_direction,
        current_row,
        current_column,
        tunnel_length + 1,
        random_length,
        max_length,
        max_tunnels
      )
    else
      map =
        change_map_value(map, current_row, current_column, %{
          value: 0,
          explored?: false,
          visible?: false
        })

      create_tunnel(
        map,
        dimensions,
        directions,
        [rand_x, rand_y],
        last_direction,
        current_row + rand_x,
        current_column + rand_y,
        tunnel_length + 1,
        random_length,
        max_length,
        max_tunnels
      )
    end
  end

  @doc """
  Changes the value of the given cell in the given map.
  """
  @spec change_map_value(map :: Map, row :: integer, column :: integer, value :: Map) :: Map
  def change_map_value(map, row, column, value, is_flipped? \\ {false, false}) do
    if row < 0 or column < 0 or row > map_size(map) or column > map_size(map) do
      map
    else
      Enum.map(map, fn {index, map_row} ->
        if index == row do
          Enum.map(map_row, fn {column_index, column_value} ->
            if column_index == column do
              new_value = Map.merge(column_value, value)

              Map.merge(new_value, %{
                rendered_tile:
                  Renderer.get_rendered_tile(map, index, column_index, new_value, is_flipped?)
              })
            else
              column_value
            end
          end)
          |> Enum.with_index(0)
          |> Enum.map(fn {column_value, column_index} -> {column_index, column_value} end)
          |> Map.new()
        else
          map_row
        end
      end)
      |> Enum.with_index(0)
      |> Enum.map(fn {row_value, row_key} -> {row_key, row_value} end)
      |> Map.new()
    end
  end

  @spec add_player(any, any) :: {:error, any} | {:ok, any}
  def add_player(map, player_value) do
    exists? =
      Enum.any?(map, fn {_row_index, row_value} ->
        Enum.any?(row_value, fn {_column_index, column_value} ->
          column_value.value == player_value
        end)
      end)

    if not exists? do
      dimensions = map_size(map)
      directions = [[-1, 0], [1, 0], [0, -1], [0, 1]]

      {:ok,
       create_start_position(map, dimensions, directions, nil, nil, [], [], player_value, false)}
    else
      {:error, map}
    end
  end

  # Creates a random start position of the player.
  # Tile must be floor, and be surrounded by 1 or 2 wall tiles.
  defp create_start_position(
         map,
         dimensions,
         directions,
         nil,
         nil,
         [],
         [],
         player_value,
         should_hide?
       ) do
    current_row = floor(:rand.uniform() * dimensions)
    current_column = floor(:rand.uniform() * dimensions)

    random_direction = find_random_direction(directions, [], [])

    create_start_position(
      map,
      dimensions,
      directions,
      current_row,
      current_column,
      random_direction,
      [],
      player_value,
      should_hide?
    )
  end

  defp create_start_position(
         map,
         dimensions,
         directions,
         current_row,
         current_column,
         random_direction,
         last_direction,
         player_value,
         should_hide?
       ) do
    if is_end_of_tunnel?(map, dimensions, current_row, current_column) do
      game_constants = Application.get_env(:master, :game_constants) |> Enum.into(%{})

      map =
        change_map_value(map, current_row, current_column, %{
          value: player_value,
          explored?: true,
          visible?: true
        })

      map = explore_around_point(map, current_row, current_column, game_constants.light_radius)

      set_visible_around_point(
        map,
        current_row,
        current_column,
        should_hide?,
        game_constants.light_radius
      )
    else
      current_row = floor(:rand.uniform() * dimensions)
      current_column = floor(:rand.uniform() * dimensions)

      create_start_position(
        map,
        dimensions,
        directions,
        current_row,
        current_column,
        random_direction,
        last_direction,
        player_value,
        should_hide?
      )
    end
  end

  def create_end_position(map, [], directions, nil, nil, [], door_value, should_hide?) do
    all_walls =
      Enum.map(map, fn {row_index, row_value} ->
        Enum.map(row_value, fn {column_index, column_value} ->
          if column_value.value == 1 do
            {row_index, column_index}
          else
            nil
          end
        end)
        |> Enum.filter(fn value -> not is_nil(value) end)
      end)

    current_row = floor(:rand.uniform() * length(all_walls))
    current_column = floor(:rand.uniform() * current_row)

    create_end_position(
      map,
      all_walls,
      directions,
      current_row,
      current_column,
      [],
      door_value,
      should_hide?
    )
  end

  def create_end_position(
        map,
        all_walls,
        directions,
        current_row,
        current_column,
        last_direction,
        door_value,
        should_hide?
      ) do
    if is_valid_end_position?(map, current_row, current_column) do
      change_map_value(map, current_row, current_column, %{
        value: door_value,
        explored?: false,
        visible?: false
      })
    else
      current_row = floor(:rand.uniform() * length(all_walls))
      current_column = floor(:rand.uniform() * current_row)

      create_end_position(
        map,
        all_walls,
        directions,
        current_row,
        current_column,
        last_direction,
        door_value,
        should_hide?
      )
    end
  end

  def manhattan_distance([x1, y1], [x2, y2]) do
    abs(x1 - x2) + abs(y1 - y2)
  end

  def euclidean_distance([x1, y1], [x2, y2]) do
    :math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
  end

  def build_path(sx, sy, tx, ty) do
    step_x =
      case tx < sx do
        true ->
          -1

        false ->
          1
      end

    step_y =
      case ty < sy do
        true ->
          -1

        false ->
          1
      end

    dx = abs((tx - sx) * 2)
    dy = abs((ty - sy) * 2)

    case dy > dx do
      true ->
        through_y(sx, sy, tx, ty, step_x, step_y, dx, dy, dx * 2 - dy, [[sx, sy]])

      false ->
        through_x(sx, sy, tx, ty, step_x, step_y, dx, dy, dy * 2 - dx, [[sx, sy]])
    end
    |> Enum.reverse()
  end

  def through_x(sx, _sy, tx, _ty, _step_x, _step_y, _dx, _dy, _f_0, p) when sx == tx do
    p
  end

  def through_x(sx, sy, tx, ty, step_x, step_y, dx, dy, f_0, p) when f_0 >= 0 do
    ny = sy + step_y
    f_1 = f_0 - dx
    nx = sx + step_x
    f_2 = f_1 + dy
    through_x(nx, ny, tx, ty, step_x, step_y, dx, dy, f_2, [[nx, ny] | p])
  end

  def through_x(sx, sy, tx, ty, step_x, step_y, dx, dy, f_0, p) when f_0 < 0 do
    ny = sy
    nx = sx + step_x
    f_2 = f_0 + dy
    through_x(nx, ny, tx, ty, step_x, step_y, dx, dy, f_2, [[nx, ny] | p])
  end

  def through_y(_sx, sy, _tx, ty, _step_x, _step_y, _dx, _dy, _f_0, p) when sy == ty do
    p
  end

  def through_y(sx, sy, tx, ty, step_x, step_y, dx, dy, f_0, p) when f_0 >= 0 do
    nx = sx + step_x
    f_1 = f_0 - dy
    ny = sy + step_y
    f_2 = f_1 + dx
    through_y(nx, ny, tx, ty, step_x, step_y, dx, dy, f_2, [[nx, ny] | p])
  end

  def through_y(sx, sy, tx, ty, step_x, step_y, dx, dy, f_0, p) when f_0 < 0 do
    nx = sx
    ny = sy + step_y
    f_2 = f_0 + dx
    through_y(nx, ny, tx, ty, step_x, step_y, dx, dy, f_2, [[nx, ny] | p])
  end

  def is_wall?(map, row, column) do
    get_map_cell_value(map, row, column).value == 1
  end

  def is_floor?(map, row, column) do
    get_map_cell_value(map, row, column).value == 0
  end

  def has_wall_between?(map, row, column, row_index, column_index) do
    list = build_path(row, column, row_index, column_index)

    walls =
      Enum.filter(list, fn [row, col] ->
        is_wall?(map, row, col)
      end)

    if length(walls) > 1 do
      true
    else
      first_wall_index = Enum.find_index(list, fn z -> z == Enum.at(walls, 0) end)

      if not is_nil(first_wall_index) and not is_nil(Enum.at(list, first_wall_index + 1)) do
        true
      else
        false
      end
    end
  end

  def explore_around_point(map, row, column, radius) do
    x_start = column - radius
    x_end = column + radius
    y_start = row - radius
    y_end = row + radius

    Enum.map(map, fn {row_index, map_row} ->
      if row_index <= y_end and row_index >= y_start do
        Enum.map(map_row, fn {column_index, column_value} ->
          if column_index <= x_end and
               column_index >= x_start and
               (manhattan_distance([row, column], [row_index, column_index]) <= 1 or
                  (manhattan_distance([row, column], [row_index, column_index]) >= 2 and
                     ((abs(row_index - row) == 1 and abs(column_index - column) < radius) or
                        (abs(row_index - row) < radius and abs(column_index - column) == 1)) and
                     not has_wall_between?(map, row, column, row_index, column_index)) or
                  (manhattan_distance([row, column], [row_index, column_index]) <= radius and
                     manhattan_distance([row, column], [row_index, column_index]) > 1 and
                     not has_wall_between?(map, row, column, row_index, column_index))) do
            Map.merge(column_value, %{explored?: true})
          else
            column_value
          end
        end)
        |> Enum.with_index(0)
        |> Enum.map(fn {column_value, column_index} -> {column_index, column_value} end)
        |> Map.new()
      else
        map_row
      end
    end)
    |> Enum.with_index(0)
    |> Enum.map(fn {row_value, row_key} -> {row_key, row_value} end)
    |> Map.new()
  end

  def set_visible_around_point(
        map,
        row,
        column,
        should_hide?,
        radius,
        is_flipped? \\ {false, false}
      ) do
    column_start = column - radius
    column_end = column + radius
    row_start = row - radius
    row_end = row + radius

    Enum.map(map, fn {row_index, map_row} ->
      if row_index <= row_end + 1 and row_index >= row_start - 1 do
        Enum.map(map_row, fn {column_index, column_value} ->
          if column_index <= column_end + 1 and
               column_index >= column_start - 1 do
            new_value =
              cond do
                manhattan_distance([row, column], [row_index, column_index]) <= 1 or
                  (manhattan_distance([row, column], [row_index, column_index]) >= 2 and
                     ((abs(row_index - row) == 1 and abs(column_index - column) < radius) or
                        (abs(row_index - row) < radius and abs(column_index - column) == 1)) and
                     not has_wall_between?(map, row, column, row_index, column_index)) or
                    (manhattan_distance([row, column], [row_index, column_index]) <= radius and
                       manhattan_distance([row, column], [row_index, column_index]) > 1 and
                       not has_wall_between?(map, row, column, row_index, column_index)) ->
                  Map.merge(column_value, %{visible?: true})

                true ->
                  case should_hide? do
                    true ->
                      Map.merge(column_value, %{visible?: false})

                    _ ->
                      column_value
                  end
              end

            Map.merge(new_value, %{
              rendered_tile:
                Renderer.get_rendered_tile(map, row_index, column_index, new_value, is_flipped?)
            })
          else
            column_value
          end
        end)
        |> Enum.with_index(0)
        |> Enum.map(fn {column_value, column_index} -> {column_index, column_value} end)
        |> Map.new()
      else
        map_row
      end
    end)
    |> Enum.with_index(0)
    |> Enum.map(fn {row_value, row_key} -> {row_key, row_value} end)
    |> Map.new()
  end

  def get_player_position(map, player_value) do
    Enum.find_value(map, fn {row_index, row_value} ->
      case player_position_in?(row_value, player_value) do
        {player_column_index, _player_column_value} -> {row_index, player_column_index}
        _ -> false
      end
    end)
    |> case do
      {row_index, player_column_index} -> {row_index, player_column_index}
      _ -> {nil, nil}
    end
  end

  defp player_position_in?(row_value, player_value) do
    Enum.find_value(row_value, fn {column_index, column_value} ->
      if column_value.value == player_value do
        {column_index, column_value}
      else
        false
      end
    end)
  end

  @spec get_map_cell_value(map :: Map, dimensions :: integer, row :: integer, column :: integer) ::
          %{
            :position => [nil | integer, ...],
            optional(any) => any
          }
  def get_map_cell_value(map, dimensions, row, column) do
    if row < 0 or column < 0 or row >= dimensions or column >= dimensions do
      %{value: "O", explored?: false, visible?: false, rendered_tile: "", position: [nil, nil]}
    else
      elem(Enum.at(elem(Enum.at(map, row), 1), column), 1)
      |> Map.merge(%{position: [row, column]})
    end
  end

  @spec get_map_cell_value(map :: Map, row :: integer, column :: integer) :: %{
          :position => [nil | integer, ...],
          optional(any) => any
        }
  def get_map_cell_value(map, row, column) do
    cond do
      is_nil(Enum.at(map, row)) ->
        %{value: "O", explored?: false, visible?: false, rendered_tile: "", position: [nil, nil]}

      is_nil(elem(Enum.at(map, row), 1)) ->
        %{value: "O", explored?: false, visible?: false, rendered_tile: "", position: [nil, nil]}

      is_nil(Enum.at(elem(Enum.at(map, row), 1), column)) ->
        %{value: "O", explored?: false, visible?: false, rendered_tile: "", position: [nil, nil]}

      is_nil(elem(Enum.at(elem(Enum.at(map, row), 1), column), 1)) ->
        %{value: "O", explored?: false, visible?: false, rendered_tile: "", position: [nil, nil]}

      true ->
        elem(Enum.at(elem(Enum.at(map, row), 1), column), 1)
        |> Map.merge(%{position: [row, column]})
    end
  end

  @spec is_end_of_tunnel?(
          map :: Map,
          dimensions :: integer,
          current_row :: integer,
          current_column :: integer
        ) :: boolean
  defp is_end_of_tunnel?(map, dimensions, current_row, current_column) do
    cond do
      # cell is a wall
      get_map_cell_value(map, dimensions, current_row, current_column).value == 1 ->
        false

      true ->
        left = get_map_cell_value(map, dimensions, current_row, current_column - 1)
        right = get_map_cell_value(map, dimensions, current_row, current_column + 1)
        top = get_map_cell_value(map, dimensions, current_row - 1, current_column)
        bottom = get_map_cell_value(map, dimensions, current_row + 1, current_column)

        neighbours =
          [left, right, top, bottom]
          |> Enum.filter(fn cell ->
            cell.value == 0
          end)

        length(neighbours) == 1 || length(neighbours) == 2
    end
  end

  defp is_valid_end_position?(map, current_row, current_column) do
    cond do
      # cell is a floor
      get_map_cell_value(map, current_row, current_column).value == 0 ->
        false

      # cell is parent
      get_map_cell_value(map, current_row, current_column).value == "P" ->
        false

      # cell is child
      get_map_cell_value(map, current_row, current_column).value == "C" ->
        false

      true ->
        left = get_map_cell_value(map, current_row, current_column - 1)
        right = get_map_cell_value(map, current_row, current_column + 1)
        bottom = get_map_cell_value(map, current_row + 1, current_column)

        # Valid door position is when left and right cell is a wall, and bottom
        # cell is a floor
        left.value == 1 and right.value == 1 and bottom.value == 0
    end
  end

  def get_neighbours(map, row, column) do
    left = get_map_cell_value(map, row, column - 1)
    right = get_map_cell_value(map, row, column + 1)
    top = get_map_cell_value(map, row - 1, column)
    bottom = get_map_cell_value(map, row + 1, column)

    left =
      if is_player?(left.value),
        do: %{
          value: 0,
          explored?: left.explored?,
          visible?: left.visible?,
          position: [row, column - 1]
        },
        else: left |> Map.merge(%{position: [row, column - 1]})

    right =
      if is_player?(right),
        do: %{
          value: 0,
          explored?: right.explored?,
          visible?: right.visible?,
          position: [row, column + 1]
        },
        else: right |> Map.merge(%{position: [row, column + 1]})

    top =
      if is_player?(top),
        do: %{
          value: 0,
          explored?: top.explored?,
          visible?: top.visible?,
          position: [row - 1, column]
        },
        else: top |> Map.merge(%{position: [row - 1, column]})

    bottom =
      if is_player?(bottom),
        do: %{
          value: 0,
          explored?: bottom.explored?,
          visible?: bottom.visible?,
          position: [row + 1, column]
        },
        else: bottom |> Map.merge(%{position: [row + 1, column]})

    %{left: left, right: right, top: top, bottom: bottom, list: [left, right, top, bottom]}
  end

  defp is_player?(value) do
    Enum.any?(["C", "P"], fn c -> c == value end)
  end

  def render_map(map) do
    Enum.map(map, fn {row_index, row_value} ->
      Enum.map(row_value, fn {column_index, column_value} ->
        Map.put(
          column_value,
          :rendered_tile,
          Renderer.get_rendered_tile(map, row_index, column_index, column_value)
        )
      end)
      |> Enum.with_index(0)
      |> Enum.map(fn {column_value, column_key} -> {column_key, column_value} end)
      |> Map.new()
    end)
    |> Enum.with_index(0)
    |> Enum.map(fn {row_value, row_key} -> {row_key, row_value} end)
    |> Map.new()
  end
end
