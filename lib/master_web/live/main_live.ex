defmodule MasterWeb.MainLive do
  use Phoenix.LiveView
  use Phoenix.HTML

  defp topic(game_uuid), do: "game:#{game_uuid}"

  def mount(_params, %{"game" => game, "current_user" => current_user} = _session, socket) do
    MasterWeb.Endpoint.subscribe(topic(game.uuid))

    stored_game = Master.GameStates.get_game_state_by_uuid(game.uuid)

    game =
      if not is_nil(stored_game) do
        stored_game
        # Map.put(stored_game, :players, stored_players)
      else
        Master.GameStates.save_game_state(game.uuid, game)
      end

    number_of_players = if is_nil(stored_game), do: 0, else: length(stored_game.players)

    if number_of_players > 2 do
      {:ok, assign(socket, game: nil)}
    else
      current_user = Master.GameStates.insert_player_to_game(game.uuid, current_user)

      if connected?(socket) do
        MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "add_player", %{
          new_player: current_user,
          game: game
        })
      end

      questioneer = Master.Questioneer.get_questioneer_questions(:PRE)

      {:ok,
       assign(socket,
         game: game,
         current_user: current_user,
         log_file: nil,
         challenge?: false,
         current_challenge: nil,
         progress: %{
           is_loading: true,
           value: 0,
           text: "Loading..."
         },
         questioneer: questioneer,
         questioneer_type: :PRE
       )}
    end
  end

  def render(assigns) do
    MasterWeb.MainView.render("main_template.html", assigns)
  end

  def handle_event("send_message", value, socket) do
    game = socket.assigns.game
    messages = socket.assigns.game.messages

    new_message = %{
      user: value["message"]["user"],
      content: value["message"]["content"]
    }

    new_messages =
      messages
      |> Kernel.++([new_message])

    game =
      game
      |> Map.put(:messages, new_messages)

    MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "send_message", %{game: game})
    {:noreply, assign(socket, game: game)}
  end

  def handle_event("create_fortnite_account", value, socket) do
    current_user = socket.assigns.current_user

    fortnite_account_details = %{
      firstname: value["fortnite_account"]["firstname"],
      lastname: value["fortnite_account"]["lastname"],
      email_address: value["fortnite_account"]["email_address"],
      dateofbirth: value["fortnite_account"]["dateofbirth"],
      username: value["fortnite_account"]["username"],
      password: value["fortnite_account"]["password"]
    }

    current_user =
      current_user
      |> Map.merge(%{fortnite_account: fortnite_account_details})

    {:noreply, assign(socket, current_user: current_user)}
  end

  def handle_event("create_map", %{"questioneer" => answers} = _, socket) do
    current_user = socket.assigns.current_user

    level = Master.Levels.get_level_by_level_number(1)

    map =
      if Map.has_key?(socket.assigns.game, :map) and socket.assigns.game.map != %{} do
        Master.MapGenerator.add_player(
          socket.assigns.game.map,
          if(current_user.child?, do: "C", else: "P")
        )
        |> elem(1)
      else
        Master.MapGenerator.create_map(
          level.dimension,
          level.max_tunnels,
          level.max_length,
          current_user.child?
        )
      end

    log_file = Master.Repo.load_log_file()

    game = socket.assigns.game

    game =
      Map.merge(game, %{
        map: map,
        level: level.number,
        required_child_points: level.required_child_points,
        required_parent_points: level.required_parent_points,
        required_total_points: level.required_total_points,
        door_opening?: false,
        door_opened?: false,
        next_level?: false
      })

    Master.GameStates.save_game_state(game.uuid, game)

    Enum.map(answers, fn {key, value} ->
      [type, question_id, option_id] = String.split(key, "_")
      option_id = if type == "radio", do: value, else: option_id

      %{
        questioneer_question_id: String.to_integer(question_id),
        questioneer_question_option_id: String.to_integer(option_id),
        answered_value: value,
        player_type: if(current_user.child?, do: :CHILD, else: :PARENT),
        game_player_id: current_user.id,
        questioneer_type: socket.assigns.questioneer_type
      }
    end)
    |> Enum.filter(fn x -> x.answered_value != "false" end)
    |> Master.Questioneer.add_questioneer_answers()

    MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "map_created", %{
      game: game,
      log_file: log_file
    })

    {:noreply, assign(socket, game: game, log_file: log_file, questioneer: nil)}
  end

  def handle_event("move_left", %{}, socket) do
    handle_event("move", %{"key" => "ArrowLeft"}, socket)
  end

  def handle_event("move_up", %{}, socket) do
    handle_event("move", %{"key" => "ArrowUp"}, socket)
  end

  def handle_event("move_right", %{}, socket) do
    handle_event("move", %{"key" => "ArrowRight"}, socket)
  end

  def handle_event("move_down", %{}, socket) do
    handle_event("move", %{"key" => "ArrowDown"}, socket)
  end

  def handle_event("move", %{"key" => key_pressed}, socket) do
    if not socket.assigns.challenge? and not socket.assigns.game.door_opening? and
         not socket.assigns.game.next_level? do
      map = socket.assigns.game.map
      player_value = if socket.assigns.current_user.child?, do: "C", else: "P"
      {player_row, player_column} = Master.MapGenerator.get_player_position(map, player_value)

      {is_valid?, new_row, new_column} =
        case key_pressed do
          "ArrowLeft" ->
            {Master.Player.Control.is_move_valid?(map, player_row, player_column - 1), player_row,
             player_column - 1}

          "ArrowUp" ->
            {Master.Player.Control.is_move_valid?(map, player_row - 1, player_column),
             player_row - 1, player_column}

          "ArrowRight" ->
            {Master.Player.Control.is_move_valid?(map, player_row, player_column + 1), player_row,
             player_column + 1}

          "ArrowDown" ->
            {Master.Player.Control.is_move_valid?(map, player_row + 1, player_column),
             player_row + 1, player_column}

          _ ->
            {false, nil, nil}
        end

      # |> IO.inspect(label: "handle_event(move) -> {is_valid?, new_row, new_column}")

      map =
        if is_valid? do
          Master.Repo.write_to_file(
            socket.assigns.log_file,
            "#{player_value} MOVED FROM [#{player_row}, #{player_column}] to [#{new_row}, #{new_column}]"
          )

          Master.GameStates.save_game_state(socket.assigns.game.uuid, socket.assigns.game)

          Master.Player.Control.move_player(
            map,
            player_row,
            player_column,
            new_row,
            new_column,
            player_value
          )
        else
          map
        end

      door_opening? =
        if is_valid? and not socket.assigns.game.door_opened? do
          top = Master.MapGenerator.get_map_cell_value(map, new_row - 1, new_column)
          top_left = Master.MapGenerator.get_map_cell_value(map, new_row - 1, new_column - 1)
          top_right = Master.MapGenerator.get_map_cell_value(map, new_row - 1, new_column + 1)

          total_points =
            Enum.reduce(socket.assigns.game.players, 0, fn player, acc -> acc + player.points end)

          child_points =
            Enum.reduce(socket.assigns.game.players, 0, fn player, acc ->
              if player.child?, do: acc + player.points, else: acc
            end)

          parent_points =
            Enum.reduce(socket.assigns.game.players, 0, fn player, acc ->
              if player.child?, do: acc, else: acc + player.points
            end)

          enough_points? =
            total_points >= socket.assigns.game.required_total_points or
              (child_points >= socket.assigns.game.required_child_points and
                 parent_points >= socket.assigns.game.required_parent_points)

          (top_left.value == "D" or top.value == "D" or top_right.value == "D") and enough_points?
        else
          false
        end

      next_level? =
        if socket.assigns.game.door_opened? and
             Master.MapGenerator.get_map_cell_value(map, new_row, new_column).value == "D" do
          true
        else
          false
        end

      # challenge? =
      #   :rand.uniform() <= 0.0001 and is_valid? and not door_opening? and not next_level?

      challenge? = :rand.uniform() <= 0.08 and is_valid? and not door_opening? and not next_level?

      current_challenge =
        if challenge? do
          random_challenge =
            Master.Challenges.get_random_challenge()
            |> Map.from_struct()
            |> Master.Helpers.atom_to_string_key_map()

          options =
            random_challenge["options"]
            |> Enum.map(fn opt ->
              Map.from_struct(opt)
              |> Master.Helpers.atom_to_string_key_map()
              |> Map.merge(%{"answered_correctly?" => false, "answered?" => false})
            end)

          Map.merge(random_challenge, %{"options" => options})
          |> Map.merge(%{"answered?" => false})
        else
          nil
        end

      # |> IO.inspect( label: "current_challenge")

      game =
        socket.assigns.game
        |> Map.merge(%{map: map, door_opening?: door_opening?, next_level?: next_level?})

      # |> IO.inspect( label: "broadcast")
      MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "player_moved", %{game: game})

      {
        :noreply,
        # |> IO.inspect( label: "return")
        assign(socket, game: game, challenge?: challenge?, current_challenge: current_challenge)
      }
    else
      {:noreply, socket}
    end
  end

  def handle_event("submit_challenge_answer", %{"challenge" => answers} = _value, socket) do
    current_user = socket.assigns.current_user
    current_challenge = socket.assigns.current_challenge

    # answeres structure:
    # Multiple: %{"1" => "true", "2" => "true", "3" => "false"}
    # Single: %{"challenge_option" => "1"}
    answered_challenge_options =
      Enum.map(current_challenge["options"], fn option ->
        if current_challenge["multiple"] do
          Map.merge(option, %{
            "answered_correctly?" => option["points"] > 0,
            "answered?" => answers["#{option["id"]}"] == "true"
          })
        else
          Map.merge(option, %{
            "answered_correctly?" => option["points"] > 0,
            "answered?" => answers["challenge_option"] == "#{option["id"]}"
          })
        end

        # if current_challenge["multiple"] do
        #   case {option["points"] > 0, answers["#{option["id"]}"] == "true"} do
        #     {true, true} ->
        #       Map.merge(option, %{"answered_correctly?" => true, "answered?" => true})

        #     {true, false} ->
        #       Map.merge(option, %{"answered_correctly?" => false, "answered?" => false})

        #     {false, true} ->
        #       Map.merge(option, %{"answered_correctly?" => false, "answered?" => true})

        #     {false, false} ->
        #       Map.merge(option, %{"answered_correctly?" => true, "answered?" => false})
        #   end
        # else
        #   case {option["points"] > 0, answers["challenge_option"] == "#{option["id"]}"} do
        #     {true, true} ->
        #       Map.merge(option, %{"answered_correctly?" => true, "answered?" => true})

        #     {true, false} ->
        #       Map.merge(option, %{"answered_correctly?" => true, "answered?" => false})

        #     {false, true} ->
        #       Map.merge(option, %{"answered_correctly?" => false, "answered?" => true})

        #     {false, false} ->
        #       Map.merge(option, %{"answered_correctly?" => true, "answered?" => false})
        #   end
        # end
      end)

    # This handle_event is called both when submitting the challenge
    # as well as when completing it (pressing "OK"). To ensure that
    # the answers and points are stored only when submitting, this
    # if statement is included
    if socket.assigns.challenge? do
      Master.Challenges.update_option_answered(answered_challenge_options)

      Enum.each(answered_challenge_options, fn answer ->
        if answer["answered_correctly?"] == true and answer["answered?"] == true do
          Master.GameStates.update_player_points(current_user, answer["points"])
        end

        if answer["answered?"] do
          %{
            challenge_id: answer["challenge_id"],
            challenge_option_id: answer["id"],
            game_player_id: current_user.id
          }
          |> Master.Challenges.add_challenge_answer()
        end
      end)
    end

    current_user = Master.GameStates.get_player_by_id(current_user.id)
    game = socket.assigns.game

    players =
      game.players
      |> Enum.map(fn player ->
        if player.id == current_user.id, do: current_user, else: player
      end)

    game = Map.put(game, :players, players)

    current_challenge =
      Map.merge(current_challenge, %{"options" => answered_challenge_options})
      |> Map.merge(%{"answered?" => true})

    {:noreply,
     assign(socket, game: game, current_challenge: current_challenge, current_user: current_user)}
  end

  def handle_event("finalise-challenge", _, socket) do
    {:noreply, assign(socket, challenge?: false)}
  end

  def handle_event("loading_assets", %{"value" => value, "text" => text}, socket) do
    progress = socket.assigns.progress

    value = progress.value + value

    progress = Map.merge(progress, %{value: value, text: text})

    progress =
      if progress.value >= 100 do
        Map.merge(progress, %{is_loading: false})
      else
        progress
      end

    {:noreply, assign(socket, progress: progress)}
  end

  def handle_event("door_opened", _value, socket) do
    # IO.inspect(socket.assigns.game, label: "handle_event(door_opened) - game", limit: :infinity)

    game =
      socket.assigns.game
      |> Map.merge(%{door_opened?: true, door_opening?: false})

    {:noreply, assign(socket, game: game)}
  end

  def handle_event("next_level", _value, socket) do
    current_user = socket.assigns.current_user
    game = socket.assigns.game
    # IO.inspect(game.players, label: "handle_event(next_level) - game.players")

    if (length(game.players) >= 2 and not current_user.child?) or length(game.players) == 1 do
      level = Master.Levels.get_level_by_level_number(game.level + 1)

      if not is_nil(level) do
        map =
          Master.MapGenerator.create_map(
            level.dimension,
            level.max_tunnels,
            level.max_length,
            current_user.child?
          )

        game =
          Map.merge(game, %{
            map: map,
            level: level.number,
            required_child_points: level.required_child_points,
            required_parent_points: level.required_parent_points,
            required_total_points: level.required_total_points,
            door_opening?: false,
            door_opened?: false,
            next_level?: false
          })

        Master.GameStates.save_game_state(game.uuid, game)

        MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "map_created", %{
          game: game,
          log_file: socket.assigns.log_file
        })

        {:noreply, assign(socket, game: game)}
      else
        game = Map.merge(game, %{game_finished?: true})

        questioneer = Master.Questioneer.get_questioneer_questions(:POST)

        MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "game_finished", %{
          game: game,
          questioneer: questioneer,
          questioneer_type: :POST
        })

        {:noreply, assign(socket, game: game, questioneer: questioneer, questioneer_type: :POST)}
      end
    else
      {:noreply, socket}
    end
  end

  def handle_event("store_choice", %{"challenge" => answers}, socket) do
    current_user = socket.assigns.current_user
    current_challenge = socket.assigns.current_challenge

    # answeres structure:
    # Multiple: %{"1" => "true", "2" => "true", "3" => "false"}
    # Single: %{"challenge_option" => "1"}
    answered_challenge_options =
      Enum.map(current_challenge["options"], fn option ->
        if current_challenge["multiple"] do
          Map.merge(option, %{
            "answered_correctly?" => false,
            "answered?" => answers["#{option["id"]}"] == "true"
          })
        else
          Map.merge(option, %{
            "answered_correctly?" => false,
            "answered?" => answers["challenge_option"] == "#{option["id"]}"
          })
        end
      end)

    current_challenge =
      Map.put(current_challenge, "options", answered_challenge_options)

    {:noreply, assign(socket, current_challenge: current_challenge)}
  end

  def handle_info(%{event: "send_message", payload: state}, socket) do
    {:noreply, assign(socket, state)}
  end

  def handle_info(%{event: "add_player", payload: state}, socket) do
    game = socket.assigns.game
    players = [state.new_player | game.players]

    game = Map.put(game, :players, players)

    MasterWeb.Endpoint.broadcast_from(self(), topic(state.game.uuid), "added_player", %{
      game: game
    })

    {:noreply, assign(socket, game: game)}
  end

  def handle_info(%{event: "added_player", payload: %{game: game}}, socket) do
    {:noreply, assign(socket, game: game)}
  end

  def handle_info(%{event: "map_created", payload: %{game: game, log_file: log_file}}, socket) do
    player_value = if socket.assigns.current_user.child?, do: "C", else: "P"

    game =
      case Master.MapGenerator.add_player(game.map, player_value) do
        {:ok, map} ->
          game = Map.merge(game, %{map: map})

          MasterWeb.Endpoint.broadcast_from(self(), topic(game.uuid), "player_added_to_map", %{
            game: game,
            log_file: log_file
          })

          game

        {:error, _map} ->
          game
      end

    {:noreply, assign(socket, game: game, log_file: log_file)}
  end

  def handle_info(
        %{event: "player_added_to_map", payload: %{game: game, log_file: log_file}},
        socket
      ) do
    {:noreply, assign(socket, game: game, log_file: log_file)}
  end

  def handle_info(%{event: "player_moved", payload: %{game: game}}, socket) do
    {:noreply, assign(socket, game: game, challenge?: socket.assigns.challenge?, current_challenge: socket.assigns.current_challenge)}
  end

  def handle_info(
        %{event: "game_finished", payload: %{game: game, questioneer: questioneer}},
        socket
      ) do
    {:noreply, assign(socket, game: game, questioneer: questioneer)}
  end
end
