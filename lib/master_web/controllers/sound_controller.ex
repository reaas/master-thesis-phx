defmodule MasterWeb.SoundController do
  use MasterWeb, :controller

  alias Master.SoundRepo

  def show_effect(conn, %{"filename" => filename}) do
    file_b64 = SoundRepo.get_effects_file(filename)

    conn
    |> put_status(200)
    |> text("data:audio/wav;base64," <> file_b64)
  end

  def show_theme(conn, %{"filename" => filename}) do
    file_b64 = SoundRepo.get_theme_file(filename)

    conn
    |> put_status(200)
    |> text("data:audio/wav;base64," <> file_b64)
  end
end
