defmodule MasterWeb.MainController do
  use Phoenix.LiveView
  use MasterWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def enter_game(conn, %{"player" => player} = _params) do
    uuid =
      if String.length(player["game_uuid"]) == 6 do
        player["game_uuid"]
      else
        Enum.random(100_000..999_999)
      end

    child? =
      case player["child?"] do
        nil -> true
        "true" -> true
        "false" -> false
        _ -> true
      end

    current_user = %{
      name: player["name"],
      child?: child?,
      fortnite_account: nil
    }

    game = %{
      uuid: uuid,
      messages: [],
      message: %{},
      players: [current_user],
      map: nil
    }

    Phoenix.LiveView.Controller.live_render(
      conn,
      MasterWeb.MainLive,
      session: %{"game" => game, "current_user" => current_user}
    )
  end
end
