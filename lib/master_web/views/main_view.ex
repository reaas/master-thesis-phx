defmodule MasterWeb.MainView do
  use MasterWeb, :view

  alias Master.MapGenerator.Renderer

  def render_game_cell(assigns, value, map, row, column) do
    ~H"""
      <%= raw(Renderer.get_rendered_tile(map, row, column, value)) %>
    """
  end
end
