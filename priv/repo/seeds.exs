# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Master.Repo.insert!(%Master.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Master.Repo
alias Master.Challenges
alias Master.Challenges.Challenge
alias Master.Challenges.ChallengeAnswer
alias Master.Challenges.ChallengeOption
alias Master.GameStates.GameState
alias Master.GameStates.GamePlayer
alias Master.GameStates.GameMessage
alias Master.Questioneer
alias Master.Questioneer.QuestioneerAnswer
alias Master.Questioneer.QuestioneerQuestion
alias Master.Questioneer.QuestioneerQuestionOption
alias Master.Levels
alias Master.Levels.Level

if Application.get_env(:master, :environment) != :prod or
     Application.get_env(:master, :reset_database) do
  Repo.delete_all(QuestioneerAnswer)
  Repo.delete_all(QuestioneerQuestionOption)
  Repo.delete_all(QuestioneerQuestion)

  Repo.delete_all(Level)

  Repo.delete_all(ChallengeAnswer)

  Repo.delete_all(GameMessage)
  Repo.delete_all(GamePlayer)
  Repo.delete_all(GameState)

  Repo.delete_all(ChallengeOption)
  Repo.delete_all(Challenge)
end

# CHALLENGES
challenges = [
  %{
    header: "Using the internet",
    multiple: true,
    text:
      "Jane uses the internet daily for studies, plays games, and browses social networking sites from her mobile phone. If Jane asks for your suggestions about using the internet, what suggestions do you think are important for Jane? For example",
    options: [
      %{
        points: 10,
        text:
          "Jane should turn off GPS location while playing the games or browsing the social networks.",
        times_answered: 0
      },
      %{
        points: 0,
        text: "Jane can use the internet as she wants; no suggestions are needed.",
        times_answered: 0
      },
      %{
        points: 20,
        text:
          "Jane should not share a lot of personal pictures and information on social networks.",
        times_answered: 0
      },
      %{
        points: 20,
        text: "Jane should regularly check her privacy setting for social networking sites.",
        times_answered: 0
      },
      %{
        points: 20,
        text: "Jane should not share her location when uploading pictures on social media.",
        times_answered: 0
      },
      %{
        points: 20,
        text: "It is not safe to use free/open wireless networks to browse the internet.",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Snapchat",
    multiple: false,
    text:
      "Suppose you have a Snapchat account. You have lots of friends on Snapchat. You regularly post pictures and videos on Snapchat and chat with your friends. If you get a friend request on Snapchat from an unknown person, would you accept it?",
    options: [
      %{
        points: 0,
        text: "Yes",
        times_answered: 0
      },
      %{
        points: 20,
        text: "No",
        times_answered: 0
      },
      %{
        points: 10,
        text: "I will ask my parents",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Snapchat",
    multiple: false,
    text:
      "Suppose you have a Snapchat account. You have lots of friends on Snapchat. You regularly post pictures and videos on Snapchat and chat with your friends. Do you think people can find out your home address from your Snapchat?",
    options: [
      %{
        points: 20,
        text: "Yes",
        times_answered: 0
      },
      %{
        points: 0,
        text: "No",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Software updates",
    multiple: true,
    text: "Why is it important to update the software on your phone, computer or gaming device?",
    options: [
      %{
        points: 10,
        text: "To ensure that the software I am using is secure.",
        times_answered: 0
      },
      %{
        points: 0,
        text:
          "There is no need to update software as it is good enough when it is initially delivered.",
        times_answered: 0
      },
      %{
        points: 10,
        text:
          "Because the updates could include important security updates that makes the software more secure.",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Passwords",
    multiple: true,
    text:
      "Suppose you are making a new social media account, and you are asked to select a strong password. Which of these passwords would you choose?",
    options: [
      %{
        points: 10,
        text: "FreshlyBakedCinnmonApplePie",
        times_answered: 0
      },
      %{
        points: 0,
        text: "your name + your birthday",
        times_answered: 0
      },
      %{
        points: 0,
        text: "qwerty",
        times_answered: 0
      },
      %{
        points: 10,
        text: "QfD8R&e4*QlIL",
        times_answered: 0
      },
      %{
        points: 0,
        text: "asdfgh",
        times_answered: 0
      },
      %{
        points: 0,
        text: "password123",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Social media",
    multiple: true,
    text:
      "If a person you do not know contacts you on social media and offers to send you money, what do you do?",
    options: [
      %{
        points: 0,
        text: "Give them your banking information, and thank them.",
        times_answered: 0
      },
      %{
        points: 5,
        text: "Ask them why they want to send you money.'",
        times_answered: 0
      },
      %{
        points: 5,
        text: "Ignore or block them.'",
        times_answered: 0
      },
      %{
        points: 10,
        text: "Talk to a parent/sibling.",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Social media",
    multiple: true,
    text:
      "If a person you do not recognise sends you a friend request on a social media platform, what is safest to do?",
    options: [
      %{
        points: 5,
        text: "Ignore it.",
        times_answered: 0
      },
      %{
        points: 10,
        text: "Decline the request and block the person.",
        times_answered: 0
      },
      %{
        points: 0,
        text: "Accept the request and contact the person to find out who they are.",
        times_answered: 0
      },
      %{
        points: 10,
        text: "Ask a parent/sibling for help.",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Sharing photos",
    multiple: true,
    text:
      "If someone you have talked to on social media asks you to send a them a naked photo, should you do it?.",
    options: [
      %{
        points: 10,
        text:
          "No, the photo could potentially be seen by others than the person you send it to, and even en up on the internet.",
        times_answered: 0
      },
      %{
        points: 0,
        text: "Yes, the photo will only be seen by the person receiving it.",
        times_answered: 0
      },
      %{
        points: 10,
        text: "I am unsure, so i would talk to a parent/sibling about it.",
        times_answered: 0
      }
    ]
  },
  %{
    header: "Social media",
    multiple: true,
    text:
      "Suppose someone you do not know sends you a message, and claims that they are a friend of your friend. If they ask you to meet with them, or where you live, what would you do.",
    options: [
      %{
        points: 0,
        text: "Give the person your address.",
        times_answered: 0
      },
      %{
        points: 5,
        text: "Answer them and get to know them better before sharing any personal information.",
        times_answered: 0
      },
      %{
        points: 10,
        text:
          "Reject the request, and block the person, because you do not know if they are who they say they are, and sharing personal information with strangers on the internet can be dangerous.",
        times_answered: 0
      }
    ]
  }
]

# QUESTIONEER
pre_questioneer_questions = [
  %{
    header: "Age",
    text: "Please enter your age",
    footer: nil,
    multiple: false,
    questioneer_type: :PRE,
    options: [
      %{
        text: nil,
        type: "integer"
      }
    ]
  },
  %{
    header: "Gender",
    text: "Please select your gender",
    footer: nil,
    multiple: false,
    questioneer_type: :PRE,
    options: [
      %{
        text: "Male",
        type: "boolean"
      },
      %{
        text: "Female",
        type: "boolean"
      },
      %{
        text: "Non-binary",
        type: "boolean"
      },
      %{
        text: "Prefer not to say",
        type: "boolean"
      }
    ]
  },
  %{
    header: "Password security",
    text: "Do you use the same passwords for several different accounts?",
    footer: nil,
    multiple: false,
    questioneer_type: :PRE,
    options: [
      %{
        text: "Yes, I have the same password for all my accounts",
        type: "boolean"
      },
      %{
        text: "Yes, sometimes",
        type: "boolean"
      },
      %{
        text: "No, I make sure to use different passwords",
        type: "boolean"
      },
      %{
        text: "I use a password manager",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "Why is it important to update all your apps and computers?",
    text: nil,
    multiple: true,
    questioneer_type: :PRE,
    options: [
      %{
        text: "In case the software company has found a security issue they have fixed",
        type: "boolean"
      },
      %{
        text: "To get the latest features of the game or app",
        type: "boolean"
      },
      %{
        text: "It is not important to update apps as they are good enough from before",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "Which of the following can be considered multi-factor authentication methods?",
    text: nil,
    multiple: true,
    questioneer_type: :PRE,
    options: [
      %{
        text: "One-time code you receive through SMS or email",
        type: "boolean"
      },
      %{
        text: "Biometric scanners (fingerprint, FaceID etc)",
        type: "boolean"
      },
      %{
        text: "A really strong password",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header:
      "How much do you think the large social media companies (Snapchat, FB, Instagram, TikTok) know about you?",
    text: nil,
    multiple: true,
    questioneer_type: :PRE,
    options: [
      %{
        text: "They know nothing about me.",
        type: "boolean"
      },
      %{
        text: "They know my name and age, but not anything else.",
        type: "boolean"
      },
      %{
        text: "They know more than I am comfortable with.",
        type: "boolean"
      },
      %{
        text:
          "They know almost everything (location, habits, personal information, relationships, interests).",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header:
      "What do you think happens to a picture when you it dissapears from social media (Snapchat, FB, Instagram, TikTok)?",
    text: nil,
    multiple: true,
    questioneer_type: :PRE,
    options: [
      %{
        text: "All copies in the company's possesion are deleted.",
        type: "boolean"
      },
      %{
        text: "The company sells the picture to other companies, but hides it from you.",
        type: "boolean"
      },
      %{
        text: "If I can't see it, it probably means that it has been deleted.",
        type: "boolean"
      },
      %{
        text: "The company keeps the picture, but hides it from you.",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header:
      "What do you think can someone do with if they have a lot of your personal information (birthday, banking information, address, name, pet names etc.)?",
    text: nil,
    multiple: true,
    questioneer_type: :PRE,
    options: [
      %{
        text: "They can not do much, because only you are you.",
        type: "boolean"
      },
      %{
        text:
          "They can more easily guess your passwords (if they are weaker), and potentially get access to your accounts and more of your information.",
        type: "boolean"
      },
      %{
        text:
          "They can order fake ids and accounts with your information and steal your identity.",
        type: "boolean"
      },
      %{
        text: "Nothing, I have nothing to hide.",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "What do you think is correct about people that you do not know on the internet?",
    text: nil,
    multiple: true,
    questioneer_type: :PRE,
    options: [
      %{
        text: "They are friendly, and usually have good intentions.",
        type: "boolean"
      },
      %{
        text: "They often have bad intentions, and want to take advantage of you.",
        type: "boolean"
      },
      %{
        text:
          "They can have criminal intentions, for example: scams, identity theft, tricking you into buying something but receiving something else or nothing at all.",
        type: "boolean"
      },
      %{
        text:
          "Most people are not bad, but there is always someone who can be evil(wants to do bad things).",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  }
]

post_questioneer_questions = [
  %{
    header: "Password security",
    text: "Do you use the same passwords for several different accounts?",
    footer: nil,
    multiple: false,
    questioneer_type: :POST,
    options: [
      %{
        text: "Yes, I have the same password for all my accounts",
        type: "boolean"
      },
      %{
        text: "Yes, sometimes",
        type: "boolean"
      },
      %{
        text: "No, I make sure to use different passwords",
        type: "boolean"
      },
      %{
        text: "I use a password manager",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "Why is it important to update all your apps and computers?",
    text: nil,
    multiple: true,
    questioneer_type: :POST,
    options: [
      %{
        text: "In case the software company has found a security issue they have fixed",
        type: "boolean"
      },
      %{
        text: "To get the latest features of the game or app",
        type: "boolean"
      },
      %{
        text: "It is not important to update apps as they are good enough from before",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "Which of the following can be considered multi-factor authentication methods?",
    text: nil,
    multiple: true,
    questioneer_type: :POST,
    options: [
      %{
        text: "One-time code you receive through SMS or email",
        type: "boolean"
      },
      %{
        text: "Biometric scanners (fingerprint, FaceID etc)",
        type: "boolean"
      },
      %{
        text: "A really strong password",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header:
      "How much do you think the large social media companies (Snapchat, FB, Instagram, TikTok) know about you?",
    text: nil,
    multiple: true,
    questioneer_type: :POST,
    options: [
      %{
        text: "They know nothing about me.",
        type: "boolean"
      },
      %{
        text: "They know my name and age, but not anything else.",
        type: "boolean"
      },
      %{
        text: "They know more than I am comfortable with.",
        type: "boolean"
      },
      %{
        text:
          "They know almost everything (location, habits, personal information, relationships, interests).",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header:
      "What do you think happens to a picture when you it dissapears from social media (Snapchat, FB, Instagram, TikTok)?",
    text: nil,
    multiple: true,
    questioneer_type: :POST,
    options: [
      %{
        text: "All copies in the company's possesion are deleted.",
        type: "boolean"
      },
      %{
        text: "The company sells the picture to other companies, but hides it from you.",
        type: "boolean"
      },
      %{
        text: "If I can't see it, it probably means that it has been deleted.",
        type: "boolean"
      },
      %{
        text: "The company keeps the picture, but hides it from you.",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header:
      "What do you think can someone do with if they have a lot of your personal information (birthday, banking information, address, name, pet names etc.)?",
    text: nil,
    multiple: true,
    questioneer_type: :POST,
    options: [
      %{
        text: "They can not do much, because only you are you.",
        type: "boolean"
      },
      %{
        text:
          "They can more easily guess your passwords (if they are weaker), and potentially get access to your accounts and more of your information.",
        type: "boolean"
      },
      %{
        text:
          "They can order fake ids and accounts with your information and steal your identity.",
        type: "boolean"
      },
      %{
        text: "Nothing, I have nothing to hide.",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "What do you think is correct about people that you do not know on the internet?",
    text: nil,
    multiple: true,
    questioneer_type: :POST,
    options: [
      %{
        text: "They are friendly, and usually have good intentions.",
        type: "boolean"
      },
      %{
        text: "They often have bad intentions, and want to take advantage of you.",
        type: "boolean"
      },
      %{
        text:
          "They can have criminal intentions, for example: scams, identity theft, tricking you into buying something but receiving something else or nothing at all.",
        type: "boolean"
      },
      %{
        text:
          "Most people are not bad, but there is always someone who can be evil(wants to do bad things).",
        type: "boolean"
      },
      %{
        text: "I don't know.",
        type: "boolean"
      }
    ]
  },
  %{
    header: "What did you think of the game?",
    text: "On a scale of 1-5, where 1 is the lowest and 5 is the highest",
    footer: nil,
    multiple: false,
    questioneer_type: :POST,
    options:
      for i <- 1..5 do
        %{text: "#{i}", type: "boolean"}
      end
  },
  %{
    header: "How fun was the game?",
    text: "On a scale of 1-5, where 1 is the lowest and 5 is the highest",
    footer: nil,
    multiple: false,
    questioneer_type: :POST,
    options:
      for i <- 1..5 do
        %{text: "#{i}", type: "boolean"}
      end
  },
  %{
    header: "How much did you enjoy playing with your parent/sibling/child?",
    text: "On a scale of 1-5, where 1 is the lowest and 5 is the highest",
    footer: nil,
    multiple: false,
    questioneer_type: :POST,
    options: [
      %{text: "I played alone.", type: "boolean"}
      | for i <- 1..5 do
          %{text: "#{i}", type: "boolean"}
        end
    ]
  },
  %{
    header: "How much do you feel you learned by playing this game?",
    text: "On a scale of 1-5, where 1 is the lowest and 5 is the highest",
    footer: nil,
    multiple: false,
    questioneer_type: :POST,
    options:
      for i <- 1..5 do
        %{text: "#{i}", type: "boolean"}
      end
  },
  %{
    header:
      "How much would you like to play this game again, but with more features, challenges and levels?",
    text: "On a scale of 1-5, where 1 is the lowest and 5 is the highest",
    footer: nil,
    multiple: false,
    questioneer_type: :POST,
    options:
      for i <- 1..5 do
        %{text: "#{i}", type: "boolean"}
      end
  }
]

levels = [
  %{
    number: 1,
    required_child_points: 60,
    required_parent_points: 60,
    required_total_points: 60,
    dimension: 20,
    max_tunnels: 50,
    max_length: 8,
    active_status: true
  },
  %{
    number: 2,
    required_child_points: 60,
    required_parent_points: 60,
    required_total_points: 60,
    dimension: 30,
    max_tunnels: 60,
    max_length: 15,
    active_status: true
  }
]

Enum.each(challenges, fn challenge ->
  if not Repo.row_exists?(Challenge, :text, challenge.text) do
    Challenges.create_challenge(challenge, challenge.options)
  end
end)

Enum.each(pre_questioneer_questions, fn question ->
  if not Repo.row_exists?(QuestioneerQuestion, :header, question.header) do
    Questioneer.create_questioneer_question(question, question.options)
  end
end)

Enum.each(post_questioneer_questions, fn question ->
  if not Repo.row_exists?(
       QuestioneerQuestion,
       :header,
       question.header,
       :questioneer_type,
       question.questioneer_type
     ) do
    Questioneer.create_questioneer_question(question, question.options)
  end
end)

Enum.each(levels, fn level ->
  if not Repo.row_exists?(Level, :number, level.number) do
    Levels.create_level(level)
  end
end)
