defmodule Master.Repo.Migrations.AddChallengeAnswerTable do
  use Ecto.Migration

  def change do
    create table("challenge_answer") do
      add :challenge_id, references(:challenge)
      add :challenge_option_id, references(:challenge_option)
      add :game_player_id, references(:game_player)

      timestamps()
    end
  end
end
