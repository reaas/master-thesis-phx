defmodule Master.Repo.Migrations.CreateBasicSchema do
  use Ecto.Migration

  def up do
    # Create PostgreSQL types
    create table("challenge") do
      add :header, :string
      add :multiple, :boolean
      add :text, :string
      add :active_status, :boolean, default: true

      timestamps()
    end

    create table("challenge_option") do
      add :challenge_id, references(:challenge)
      add :points, :integer
      add :text, :string
      add :times_answered, :integer, default: 0
      add :active_status, :boolean, default: true

      timestamps()
    end

    create table("questioneer_question") do
      add :header, :string
      add :text, :string
      add :footer, :string
      add :multiple, :boolean
      # PRE, POST
      add :questioneer_type, :string
      add :active_status, :boolean, default: true

      timestamps()
    end

    create table("questioneer_question_option") do
      add :questioneer_question_id, references(:questioneer_question)
      add :text, :string
      # boolean, string, integer
      add :type, :string
      add :active_status, :boolean, default: true

      timestamps()
    end

    create table("game_state") do
      add :uuid, :string
      add :map, :map, default: %{}
      add :level, :integer, default: 1
      add :door_opening?, :boolean, default: false
      add :game_finished?, :boolean, default: false

      timestamps()
    end

    create table("game_message") do
      add :game_state_id, references(:game_state)
      add :user, :string
      add :content, :string

      timestamps()
    end

    create table("game_player") do
      add :game_state_id, references(:game_state)
      add :child?, :boolean
      add :name, :string
      add :points, :integer, default: 0

      timestamps()
    end

    create table("questioneer_answer") do
      add :questioneer_question_id, references(:questioneer_question)
      add :questioneer_question_option_id, references(:questioneer_question_option)
      add :answered_value, :string
      # CHILD, PARENT
      add :player_type, :string
      add :game_player_id, references(:game_player)
      # PRE, POST
      add :questioneer_type, :string

      timestamps()
    end

    create table("level") do
      add :number, :integer
      add :required_child_points, :integer
      add :required_parent_points, :integer
      add :required_total_points, :integer
      add :dimension, :integer, default: 20
      add :max_tunnels, :integer, defualt: 50
      add :max_length, :integer, default: 8
      add :active_status, :boolean, default: true

      timestamps()
    end
  end

  def down do
    drop table("challenge")
    drop table("challenge_option")
    drop table("questioneer_question")
    drop table("questioneer_question_option")
    drop table("questioneer_answer")
    drop table("game_state")
    drop table("game_message")
    drop table("game_player")
    drop table("level")
  end
end
