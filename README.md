# Master Thesis PHX

Update README.md to include how to setup heroku, how to extract data, how to enable/disable reset of db ++?
Should we share the data? If so: where? Figshare?

## Setup

### Database, `postgresql`
Follow the [instructions](https://www.postgresql.org/download/) found on the official `postgresql`-website to install `postgresql` on your local machine.

After installation and setup, create a database called `master` in the `postgresql`-cli:
```sql
CREATE DATABASE master;
```
#### On linux
Open a terminal and type
```bash
sudo -u postgres psql
```
When inside the postgresql session run the following command to set the password for the "postgres" user:
```postgresql
\password postgres
\*The password you want\*(this should match the password in dev.exs)
```

### `asdf`, `elixir` and `erlang`
`asdf` ([offcial website](https://asdf-vm.com/)) is a tool version management system that lets you easily install, run and manage different versions of a large number of runtimes and tools. This project used the following `asdf-plugins` (these are found in [.tool-versions](.tool-versions) in the root of this project):
```bash
elixir 1.13.4-otp-24
erlang 24.3.3
```

To install `asdf`, please follow the [instructions](https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies) on the official `asdf`-website (make sure you have the necessary requirements listed in the instructions).

*NOTE: if you are running windows, we strongly recommend downloading [git for windows](https://gitforwindows.org/) and download `asdf` using Git, and installing it using the "Bash & Git" instructions on the website*

After `asdf` is installed, run the following:
```bash
asdf plugin add erlang
asdf plugin add elixir
asdf install erlang 24.3.3
asdf install elixir 1.13.4-otp-24
```

You do not have to clone this repository before doing the above. `asdf` will automatically use the runtime versions described in [.tool-versions](.tool-versions) if they are installed.

### Running the application
```bash
# Cloning the repository (HTTPS)
git clone https://gitlab.stud.idi.ntnu.no/reaas/master-thesis-phx.git
# Optionally clone the repository over SSH
git clone git@gitlab.stud.idi.ntnu.no:reaas/master-thesis-phx.git

# Navigate to the directory
cd master-thesis-phx
# Install dependencies, create database and fill database with data
mix setup
# Run the application
mix phx.server
```

The application should now be available in your browser on [localhost:4000](localhost:4000).

## Assets used
Dungeon Crawler pixel pack fetched from https://o-lobster.itch.io/simple-dungeon-crawler-16x16-pixel-pack

Mythic woods pixel pack fetched from https://game-endeavor.itch.io/mystic-woods

SVL's RPG Music Pack fetched from https://svl.itch.io/rpg-music-pack-svl
